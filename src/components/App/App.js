import React, { Component } from 'react';
import Blank from '../Blank/Blank';
import Friendrank from '../Friendrank/Friendrank';
import PaySuccess from '../PaySuccess/PaySuccess';
import BuyFail from '../BuyFail/BuyFail';
import SingleTreasureBox from '../SingleTreasureBox/SingleTreasureBox';
import TravelSecurity from '../TravelSecurity/TravelSecurity';

import './App.css';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
          <Router>
              <Switch>
                  <Route exact path="/" component={ PaySuccess} />
                  <Route path="/Blank" component={Blank} />
                  <Route path="/Friendrank" component={Friendrank} />
                  <Route path="/BuyFail" component={BuyFail} />
                  <Route path="/SingleTreasureBox" component={SingleTreasureBox} />
                  <Route path="/TravelSecurity" component={TravelSecurity} />
              </Switch>
          </Router>
      </div>
    );
  }
}

export default App;
